<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\userProfile
{
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;
	use nuclio\plugin\config\Config;
	use nuclio\plugin\config\ConfigCollection;
	use nuclio\plugin\database\datasource\source\Source;
	use nuclio\plugin\userProfile\model\Profile as ProfileModel;
	use nuclio\plugin\userProfile\model\ProfileField as ProfileFieldModel;
	use nuclio\plugin\database\orm\Model;

	/**
	 * User profile class responsible for maintaining user profile data
	 * 
	 * @package    nuclio\plugin\userProfile
	 */
	class UserProfile extends Plugin
	{
		/**
		 * @var        Source $dataSourceManager 	An instance od Data Source Manager class
		 * 
		 * @access private
		 */
		private Source $dataSourceManager;

		/**
		 * Static method to create an instance of UserProfile class 
		 * 
		 * Create an instance of UserProfile class if there is no existing one
		 * 
		 * @access public 
		 * 
		 * @return     UserProfile An instance of UserProfile class
		 */
		public static function getInstance(/* HH_FIXME[4033] */...$args):UserProfile
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}

		public function __construct(Source $dataSource)
		{
			$this->dataSourceManager=$dataSource;
			parent::__construct();
		}

		/**
		 * Method to add a field to the profile collection
		 * 
		 * @access public
		 * 
		 * @param      UserProfile $addField Add new field label for user profile field
		 * 
		 * @return     UserProfile An instance of UserProfile with new data
		 */
		public function addField(string $field):this
		{
			$label = ucwords($field);
			$ref   = str_replace(' ', '_', strtolower($field));
			$profileFieldMap = Map 
			{
				'label' => $label,
				'ref'	=> $ref
			};
			// Find field that already created
			$profileField = ProfileFieldModel::findOne
			(
				Map 
				{
					'ref'=>$ref
				}
			);
			if(is_null($profileField))
			{
				// If empty, create a new one
				$profileField = ProfileFieldModel::create($profileFieldMap);
				$profileField->save();
			}
			return $this;
		}

		/**
		 * Method to save the use profile, 
		 * the user profile field must be added first, otherwise it will be ignored
		 * 
		 * @access public
		 * 
		 * @param      mixed $user User data
		 * @param      Map|null $profileField profile data which need to be saved
		 * 
		 * @return     UserProfile An instance of User profile with new data
		 */
		public function saveProfile(mixed $user, ?Map $profileFields = null):this
		{
			if($user instanceof Model)
			{
				$profile = ProfileModel::findOne(Map{'user'=>$user});
			}
			else
			{
				$profile = ProfileModel::findOne
						(
							Map
							{
								'user._orm._id'=>$user
							}
						);
				$user = UserModel::findById($user);
			} 
			
			if(is_null($profile))
			{
				$profile=ProfileModel::create();
				$profile->setUser($user);
			}
			if($profileFields instanceof Map)
			{
				foreach($profileFields as $profileKey => $profileData)
				{
					$profileField = ProfileFieldModel::findOne(Map{'ref'=>$profileKey});
					if(!is_null($profileField))
					{
						$setter = 'set'.ucfirst($profileKey);
						$profile->$setter($profileData);
					}
				}
			}
			$profile->save();

			return $this;
		}

		/**
		 * Method to get user profile by User data model
		 * 
		 * @access public
		 * 
		 * @param      Model $user (description)$user Model of user data
		 * 
		 * @return     Map|null User profile data
		 */
		public function getProfileByUser(Model $user):?Map
		{
			$profile = ProfileModel::findOne(Map{'user'=>$user});

			if($profile)
			{
				return $this->toMap($profile);
			}
			return null;
		}

		/**
		 * Method to get user profile by user Id
		 * 
		 * @access public
		 * 
		 * @param      string $id User ID
		 * 
		 * @return     Map|null User profile data
		 */
		public function getProfileByUserId(string $id):?Map
		{
			$profile = ProfileModel::findOne
						(
							Map
							{
								'user._orm._id'=>$id
							}
						);

			if($profile)
			{
				return $this->toMap($profile);
			}
			return null;
		}

		/**
		 * Method to convert the data model to a Map
		 * 
		 * @access private
		 * 
		 * @param      Model $profile User profile object
		 * 
		 * @return     Map Map object of user profile model
		 */
		private function toMap(Model $profile):Map
		{
			$profile=$profile->toMap();
			$profile['user']=$profile->get('user')->toMap();

			return $profile;
		}
	}
}
