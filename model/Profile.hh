<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\userProfile\model
{ 
	use nuclio\plugin\database\orm\DynamicModel;
	use nuclio\plugin\database\orm\Model;
	
	/**
	 * @Collection profile
	 * 
	 * user Profile data model
	 * 
	 * represent user profile cllection in database
	 * 
	 * @package    nuclio\plugin\userProfile\model
	 */
	class Profile extends DynamicModel
	{
		/**
		 * @Id(strategy="AUTO")
		 * 
		 * @var        mixed $id 	User profile ID
		 * 
		 * @access public 
		 */
		public mixed $id=null;

		/**
		 * @relate nuclio\plugin\user\model\User
		 * 
		 * @var        Model|null $user 	User model object 
		 * 
		 * @access public
		 */
		public ?Model $user=null;
	}
}
