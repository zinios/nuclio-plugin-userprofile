<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\userProfile\model
{ 
	use nuclio\plugin\database\orm\Model;
	
	/**
	 * @Collection profileField
	 * 
	 * User profile detail data model
	 * 
	 * represent profile filed collection in database
	 * 
	 * @package    nuclio\plugin\userProfile\model
	 */
	class ProfileField extends Model
	{ 
		/**
		 * @Id(strategy="AUTO")
		 * 
		 * @var        mixed $id 	User profile fireld ID
		 * 
		 * @access public
		 */
		public mixed $id=null;

		/**
		 * @var        string $label 	The title of profile widget
		 * 
		 * @access public
		 */
		public string $label=null;

		/**
		 *@var        string $ref 	The reference of data
		 *
		 *@access public
		 */
		public string $ref=null;

		/**
		 * @var        string $type 	The type of widget
		 *
		 *@access public
		 */
		public string $type=null;
	}
}
